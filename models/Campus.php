<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "campus".
 *
 * @property int $id_campus
 * @property string $Nombre
 * @property string $Calle
 * @property string $Colonia
 * @property int $Numero
 * @property int $Codigo_postal
 * @property int $Telefono
 * @property string $Ciudad
 * @property string $Estado
 *
 * @property Departamentos[] $departamentos
 */
class Campus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_campus', 'Nombre', 'Calle', 'Colonia', 'Numero', 'Codigo_postal', 'Telefono', 'Ciudad', 'Estado'], 'required'],
            [['id_campus', 'Numero', 'Codigo_postal', 'Telefono'], 'integer'],
            [['Nombre', 'Calle', 'Colonia', 'Ciudad', 'Estado'], 'string', 'max' => 50],
            [['id_campus'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_campus' => 'Id Campus',
            'Nombre' => 'Nombre',
            'Calle' => 'Calle',
            'Colonia' => 'Colonia',
            'Numero' => 'Numero',
            'Codigo_postal' => 'Codigo Postal',
            'Telefono' => 'Telefono',
            'Ciudad' => 'Ciudad',
            'Estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Departamentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentos()
    {
        return $this->hasMany(Departamentos::className(), ['id_campus' => 'id_campus']);
    }
}
