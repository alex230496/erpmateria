<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "departamentos".
 *
 * @property int $id_departamento
 * @property string $Nombre
 * @property string $Descripcion
 * @property int $Telefono
 * @property int $id_campus
 *
 * @property Campus $campus
 * @property Personal[] $personals
 */
class Departamentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departamentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_departamento', 'Nombre', 'Descripcion', 'Telefono', 'id_campus'], 'required'],
            [['id_departamento', 'Telefono', 'id_campus'], 'integer'],
            [['Nombre'], 'string', 'max' => 50],
            [['Descripcion'], 'string', 'max' => 100],
            [['id_departamento'], 'unique'],
            [['id_campus'], 'exist', 'skipOnError' => true, 'targetClass' => Campus::className(), 'targetAttribute' => ['id_campus' => 'id_campus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_departamento' => 'Id Departamento',
            'Nombre' => 'Nombre',
            'Descripcion' => 'Descripcion',
            'Telefono' => 'Telefono',
            'id_campus' => 'Id Campus',
        ];
    }

    /**
     * Gets query for [[Campus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampus()
    {
        return $this->hasOne(Campus::className(), ['id_campus' => 'id_campus']);
    }

    /**
     * Gets query for [[Personals]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPersonals()
    {
        return $this->hasMany(Personal::className(), ['id_departamento' => 'id_departamento']);
    }
}
