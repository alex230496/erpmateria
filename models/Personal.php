<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personal".
 *
 * @property int $id_personal
 * @property string $Nombre
 * @property string $Apellido_paterno
 * @property string $Apellido_materno
 * @property string $Fech_nacimiento
 * @property string $Sexo
 * @property string $RFC
 * @property int $Status
 * @property int $id_departamento
 *
 * @property Departamentos $departamento
 */
class Personal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_personal', 'Nombre', 'Apellido_paterno', 'Apellido_materno', 'Fech_nacimiento', 'Sexo', 'RFC', 'Status', 'id_departamento'], 'required'],
            [['id_personal', 'Status', 'id_departamento'], 'integer'],
            [['Fech_nacimiento'], 'safe'],
            [['Nombre', 'Apellido_paterno', 'Apellido_materno'], 'string', 'max' => 50],
            [['Sexo', 'RFC'], 'string', 'max' => 30],
            [['id_personal'], 'unique'],
            [['id_departamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamentos::className(), 'targetAttribute' => ['id_departamento' => 'id_departamento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_personal' => 'Id Personal',
            'Nombre' => 'Nombre',
            'Apellido_paterno' => 'Apellido Paterno',
            'Apellido_materno' => 'Apellido Materno',
            'Fech_nacimiento' => 'Fech Nacimiento',
            'Sexo' => 'Sexo',
            'RFC' => 'Rfc',
            'Status' => 'Status',
            'id_departamento' => 'Id Departamento',
        ];
    }

    /**
     * Gets query for [[Departamento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamento()
    {
        return $this->hasOne(Departamentos::className(), ['id_departamento' => 'id_departamento']);
    }
}
